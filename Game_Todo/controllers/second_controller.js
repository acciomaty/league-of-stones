
app.directive('carte', function () {
    return {
        replace: false,     

        link: function($scope, elem, attrs) {
            
            
          

            var card,
                image,
                cardw,
                cardh,
                cardx,
                cardy,
                ocardx,
                ocardy,
                pinx,
                piny,
                pinxperc,
                pinyperc,
                targetx,
                targety,
                rx,
                ry,
                targetrx,
                targetry,
                scale,
                targetscale,
                ww,
                wh,
                md,
                mx,
                my,
                whoosh,
                whooshvol,
                whooshvoltarget,
                majesty,
                majestyvol,
                majestyvoltarget,
                audioloaded;
                var firstleft = ((attrs.id * 7) - 20 )
                var firsttop= 30
                var disttop;
                var distleft;
                var premier=0;
                var again=false;


            // a changer pour aller sur le bon element
            board = elem.parent().parent()[0].childNodes[3];


            launch();
            bindevents();
            loop();
           
            // we get the board by getting parents element
            
           


            function launch() {

                onresize();

                
                card = elem[0];
                image = elem[0].childNodes[1];
                
                cardw = image.width;
                cardh = image.height;
                cardx = ww / 2 - cardw / 2;
                cardy = wh / 2 - cardh / 2;
                ocardx = cardx;
                ocardy = cardy;
                pinx = 0;
                piny = 0;
                pinxperc = 0;
                pinyperc = 0;
                targetx = cardx;
                targety = cardy;
                rx = 0;
                ry = 0;
                targetrx = 0;
                targetry = 0;
                scale = 1;
                targetscale = scale;
                md = false;
                mx = cardx;
                my = cardy;
               
    
            }

            function overlaps( a, b ) {
                    var pos1 = getPositions( a ),
                        pos2 = getPositions( b );
                    return comparePositions( pos1[0], pos2[0] ) && comparePositions( pos1[1], pos2[1] );
                }




                function getPositions( elem ) {
                    var pos, width, height;
                    pos = $( elem ).position();
                    width = $( elem ).width();
                    height = $( elem ).height();
                    return [ [ pos.left, pos.left + width ], [ pos.top, pos.top + height ] ];
                }

                function comparePositions( p1, p2 ) {
                    var r1, r2;
                    r1 = p1[0] < p2[0] ? p1 : p2;
                    r2 = p1[0] < p2[0] ? p2 : p1;
                    return r1[1] > r2[0] || r1[0] === r2[0];
                }

              

            function bindevents() {

                //for pc
                elem.bind( 'mousedown', onmousedown );
                window.addEventListener( 'mouseup', onmouseup );
                window.addEventListener( 'mousemove', onmousemove );

                //for phone
                elem.bind('touchstart',onmousedown);
                window.addEventListener('touchend',onmouseup);
                window.addEventListener('touchmove',onmousemove);



                window.addEventListener( 'resize', onresize );
                
                
                card.style.top= firsttop+'%'
                card.style.left = firstleft+'%'
                
            }


            function onmousedown( e ) {
                
                md = true;
                mx = e.pageX;
                my = e.pageY;
                // pin to click point
                pinx = mx - cardx; 
                piny = my - cardy; 
                // transform based on the pin position
                pinxperc = 100 - ( pinx / cardw ) * 100; 
                pinyperc = 100 - ( piny / cardh ) * 100; 
            }

            function onmouseup() {
                md = false;

               
                //card played now!
                if (overlaps (board,card)){
                    unid= card.id
                    unimg = image.src
                    parent = card.parentNode
                    
                   
                    
                    if($scope.ourBattlefield.length < 5){
                        parent.removeChild(card);
                        $scope.ourBattlefield.push( {
                            id: card.id,
                            img: image.src,
                        });


                     obj= $scope.playablecard.find(function(el){
                            return el.id == unid;
                     });

                    idaenlever =$scope.playablecard.indexOf(obj);
                    $scope.playablecard.splice(idaenlever,1)


                        // will be done when a new card will be created    
                     }else{
                        mx= disttop +pinx;
                        my= distleft+piny;
                     }
                      $scope.$apply();
                }



            }

            function onmousemove( e ) {
                if( md ) {
                    mx = e.pageX;
                    my = e.pageY;
                }
            }

            function onresize() {
                ww = window.innerWidth;
                wh = window.innerHeight;
            }

            function loop() {
                requestAnimationFrame( loop )

                if(premier == 0){
                    distleft = my;
                    disttop = mx;
                    premier = 1;
                }
                

                    // set new target position
                    targetx = mx - cardx - pinx;
                    targety = my - cardy - piny;
                
                
                // lerp to new position
                cardx += targetx * 0.25;
                cardy += targety * 0.25;
                
                // contain card to window bounds
                if( cardx < -cardw / 2 ) {
                    cardx = -cardw / 2;
                }
                if( cardx > ww - cardw / 2 ) {
                    cardx = ww - cardw / 2;
                }
                if( cardy < -cardh / 2 ) {
                    cardy = -cardh / 2;
                }
                if( cardy > wh - cardh / 2 ) {
                    cardy = wh - cardh / 2;
                }
                
                // get rotation based on how much card moved
                targetrx = ( ocardy - cardy - rx ) * 2;
                targetry = ( cardx - ocardx - ry ) * 2;
                
                // lock rotation so things don't get too crazy
                targetrx = Math.min( targetrx, 40 );
                targetrx = Math.max( targetrx, -40 );
                targetry = Math.min( targetry, 40 );
                targetry = Math.max( targetry, -40 );
                
                // lerp to new rotation
                rx += targetrx * 0.1;
                ry += targetry * 0.1;
                    
                // scale up when the mouse is pressed
                targetscale = md ? 1.9 - scale : 1 - scale;
                scale += targetscale * 0.2;
                
                // apply the transform
                card.style[ 'transform' ] = 'translate3d(' + cardx + 'px, ' + cardy + 'px, 0)';
                image.style[ 'transform-origin' ] = pinxperc + '% ' + pinyperc + '%';
                image.style[ 'transform' ] = 'scale(' + scale + ') rotateY(' + ry + 'deg) rotateX(' + rx + 'deg)';

                // store the old card position
                ocardx = cardx;
                ocardy = cardy;
            }





            





        }
    };
});



app.controller('hand', ['$scope', function($scope) {



    $scope.playablecard = [
        {
            id: 0,
            img : "https://s3.amazonaws.com/cardgenhs/t/bIaYGwRl.png",
        },
        

        {
            id: 1,
            img : "https://s3.amazonaws.com/cardgenhs/t/bIaYGwRl.png",
        },
        {
            id: 2,
            img : "https://s3.amazonaws.com/cardgenhs/t/bIaYGwRl.png",
        },
        
    ];   

    $scope.originalhand = angular.copy($scope.playablecard); 




 }]);









  /*  app.controller('board', ['$scope','$element', function($scope,$element) {
        $scope.getNumber = function(num) {
            return new Array(num);   
        }


        $scope.carteAdverse = 6;

        $scope.elemSelected = ''
        
        //leBoard = $element[0].children[0];

        // we need to convert the json to array so we can use track by $index in ng-repeat

        $scope.ourBattlefield= [{id:4,img : "https://s3.amazonaws.com/cardgenhs/t/bIaYGwRl.png",},{id:3,img : "https://s3.amazonaws.com/cardgenhs/t/bIaYGwRl.png",},{id:5,img : "https://s3.amazonaws.com/cardgenhs/t/bIaYGwRl.png",},]


        $scope.ennemyBattlefield= [{id:0,img :"https://s3.amazonaws.com/cardgenhs/t/k98ZQ8hp.png"}]

        $scope.played=[]


        $scope.decks = [{
                id: 1,
                img : "img/dos_carte.png",
                top : 25,
                cardleft: 30,
            },{
                id: 2,
                img : "img/dos_carte.png",
                top: 100-45,        
                cardleft: 30,
            
            }
        ];   





    $scope.attack= function(card){
        console.log("c'est obj 1 qui attaque obj2",$scope.cardSelected,card);
        
        $scope.mainPlayerAction= false;
        $scope.elemSelected.style.border='1px solid black';
        $scope.cardSelected = '';
        $scope.elemSelected ='';
    }



    $scope.mainPlayerAction = false;

    $scope.cardActivate= function(card,event){
        if(!$scope.mainPlayerAction){
            $scope.elemSelected = event.target
            $scope.cardSelected = card;
            $scope.mainPlayerAction= true;
            event.target.style.border ="1px solid red" ;
        }else{

            if($scope.elemSelected == event.target){
                $scope.mainPlayerAction= false;
                $scope.elemSelected.style.border='1px solid black';
                $scope.cardSelected = '';
                $scope.elemSelected ='';
        

            }

        }
    };*/



/*

 }]);*/


app.directive('deck', [function() {
    return {
        
        replace: false,
        link: function(scope, elm, attrs) {
            elm.on('mouseenter',function() {
                    elm.css('opacity','1');
                });

            elm.on('mouseleave',function() {
                    elm.css('opacity','0');
                });
        }
    };
}]);




//responsive: board avec image/event/positiondepart card





//test pour les events:
app.directive('basicClick', function($parse, $rootScope) {
  return {
    compile: function(elem, attr) {
      var fn = $parse(attr.basicClick);
      return function(scope, elem) {
        elem.on('click', function(e) {
          fn(scope, {$event: e});
          scope.$apply();
        });
      };
    }
  };
});






app.directive('playingGame', function () { 
    return {
        restrict: 'A',
        replace: true,
        scope : true, 
        templateUrl: 'view/playingGame.html',
        link: function($scope,$element) {

            $scope.getNumber = function(num) {
                return new Array(num);   
            }


            $scope.carteAdverse = 6;

            $scope.elemSelected = ''
            
            //leBoard = $element[0].children[0];

            // we need to convert the json to array so we can use track by $index in ng-repeat

            $scope.ourBattlefield= [{id:4,img : "https://s3.amazonaws.com/cardgenhs/t/bIaYGwRl.png",},{id:3,img : "https://s3.amazonaws.com/cardgenhs/t/bIaYGwRl.png",},{id:5,img : "https://s3.amazonaws.com/cardgenhs/t/bIaYGwRl.png",},]


            $scope.ennemyBattlefield= [{id:0,img :"https://s3.amazonaws.com/cardgenhs/t/k98ZQ8hp.png"}]

            $scope.played=[]


            $scope.decks = [{
                    id: 1,
                    img : "img/dos_carte.png",
                    top : 25,
                    cardleft: 30,
                },{
                    id: 2,
                    img : "img/dos_carte.png",
                    top: 100-45,        
                    cardleft: 30,
                
                }
            ];   





            $scope.attack= function(card){
                console.log("c'est obj 1 qui attaque obj2",$scope.cardSelected,card);
                /*
                ... on effectue l'action avec l'appel
                */
                $scope.mainPlayerAction= false;
                $scope.elemSelected.style.border='1px solid black';
                $scope.cardSelected = '';
                $scope.elemSelected ='';
            }



            $scope.mainPlayerAction = false;

            $scope.cardActivate= function(card,event){
                if(!$scope.mainPlayerAction){
                    $scope.elemSelected = event.target
                    $scope.cardSelected = card;
                    $scope.mainPlayerAction= true;
                    event.target.style.border ="1px solid red" ;
                }else{

                    if($scope.elemSelected == event.target){
                        $scope.mainPlayerAction= false;
                        $scope.elemSelected.style.border='1px solid black';
                        $scope.cardSelected = '';
                        $scope.elemSelected ='';
                    }

                }
            };
        }
    };
});