﻿/*
 * This provider grants access to the connected user's data.
 *
 * The name used by non-connected users can be changed thanks to userProvider.defaultName
 */
app.provider('user', function() {
 
    this.defaultName = "valeureux combattant";
    
    this.$get = function() {
        
        user = {
            name: "",
            token: "",
            email: "",
            pwd: "",
            isConnected: false
        }
        
        // utility functions
        user.set = function(name, email, token) {
            user.pwd = "";  // never store the password
            
            user.name = name;
            user.email = email;
            user.token = token;
            user.isConnected = true;
                
            sessionStorage.setItem('token', token);
            sessionStorage.setItem('username', name);  
            sessionStorage.setItem('email', email);
        }
        
        user.unset = function() {
            user.set("", "", "");
            
            user.name = this.defaultName;
            user.isConnected = false;
        }

        // handle refresh
        user.name = sessionStorage.getItem("username");
    
        // the user is not connected
        if (user.name == null || user.name == "") {
            user.name = this.defaultName;
        }
        // the user was connected
        else {
            user.token = sessionStorage.getItem("token");
            user.email = sessionStorage.getItem("email");
            user.isConnected = true;
        }
        
        return user;
    }
    
});