﻿/*
 * This provider grants access to the connected user's data.
 *
 * The name used by non-connected users can be changed thanks to userProvider.defaultName
 */
app.provider('deck', function() {
    
    class Deck {
        constructor(name, decks) {
            this.decks = decks;
            this.name = name;
            this.cards = [];
        }
        
        get cards() {
            return this.cards;
        }
        
        get name() {
            return this.name;
        }
        
        addCard(card) {
            this.cards.push(card);
            if( this.decks )
                this.decks.persist();
        }
        
        removeCard(card) {
            this.cards.splice(this.cards.indexOf(card), 1);
            
            if( this.decks )
                this.persist(deck);
        }
    }
    
    this.persist = (deck) => {
        window.sessionStorage.setItem("decks", JSON.stringify(deck.decks));
    }
    
    this.reload = (deck) => {
        deck.decks = JSON.parse(window.sessionStorage.getItem("decks"));
        
        if( deck.decks == undefined )
            deck.decks = JSON.parse(window.localStorage.getItem("decks"));
        
        if( deck.decks == undefined )
            deck.decks = [];
        
        return deck.decks;
    }
    
    this.addUtilityMethods = (new_deck, deck) => {
        new_deck.addCard = (card) => {
            new_deck['cards'].push(card);
            
            if( deck )
                this.persist(deck);
        }
        
        new_deck.removeCard = (card) => {
            new_deck.splice(deck.indexOf(card), 1);
            
            if( deck )
                this.persist(deck);
        }
        
        return deck;
    }
    
    // used to cache the cards/getAll request
    this.allCards = null;
 
    this.$get = function($http) {
        
        deck = {
            decks: []
        }
        
        // handle refresh
        this.reload(deck); 
        
        deck.create = (name) => {
            new_deck = addUtilityMethods({
                'name': name,
                'cards': []
            }, deck);
            
            deck.decks.push(new_deck);
            this.persist(deck);
            
            return new_deck;
        }
        
        deck.createIndependant = (name) => {
            return addUtilityMethods( {
                'name': name,
                'cards': []
            }, null);
        }
        
        deck.remove = (deck_to_remove) => {
            deck.decks = deck.decks.filter(function(deck) {
                return deck.name != deck_to_remove.name;
            });
            
            console.log('removing');
        
            this.persist(deck);
        }
        
        deck.save = () => {
            window.localStorage.setItem("decks", JSON.stringify(deck.decks));
        }
        
        deck.getAllCards = () => {
            if( ! this.allCards ) {
                $http.get("https://amarger.murloc.fr:8080/cards/getAll?token=" + user.token)
                    .then(function(response){
                        if (response.data.status == "error") {
                            console.log("getting cards failed : " + response.data.message)
                            return;
                            
                        this.allCards = response.data.data;
                    }
                });
            }

            return this.allCards
        }
        
        return deck;
    }
    
});