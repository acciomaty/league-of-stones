﻿
/*
 * This service :
 *      - manages the connection to the server
 *      - updates `user` data according to server's responses
 */
app.service('connection', ['$http', '$rootScope', 'user', 'messages', function($http, $rootScope, user, messages) {
    
    this.connect = function(user) {
        $http.get("https://amarger.murloc.fr:8080/users/connect?email=" + user.email + "&password=" + user.pwd)
            .then(function(response){
                 
                if (response.data.status == "error") {
                    messages.setError('Erreur de connexion', response.data.message);
                    return;
                }
                
                // clear pwd
                user.pwd = "";
                    
                // update user data
                user.set(response.data.data.name, response.data.data.email, response.data.data.token);
                $rootScope.$broadcast('connection');
          });
    };
    
    this.disconnect = function() {
        $http.get("https://amarger.murloc.fr:8080/users/disconnect?token="+ user.token)
            .then(function(response){
                
                if (response.data.status == "error") {
                    console.log("disconnect failed")
                    return;
                }
                
                // clear user data
                user.unset();
                
                $rootScope.$broadcast('disconnection');
            });
    };
    
    this.suscribe = function(suscriber) {
        $http.get("https://amarger.murloc.fr:8080/users/subscribe?email=" + suscriber.email + "&name=" + suscriber.pseudo + "&password=" + suscriber.pwd)
            .then(function(response){
                
                if (response.data.status == "error") {
                    messages.setError("Erreur d'inscription", response.data.message);
                    return;
                }
                
                // clear user data
                user.unset();
                
                $rootScope.$broadcast('subscription', response.data);
        });
    };
    
    this.delete_account = function() {
        $http.get("https://amarger.murloc.fr:8080/users/unsubscribe?email=" + user.email + "&password=" + user.pwd + "&token=" + user.token)
            .then(function(response){
                
                if (response.data.status == "error") {
                    messages.setError("Impossible de supprimer le compte", response.data.message);
                    return;
                }
                
                // clear user data
                user.unset();
                
                $rootScope.$broadcast('delete_account', response.data);
        });
    }
}]);