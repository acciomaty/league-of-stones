﻿/*
 * This provider grants access to the connected user's data.
 *
 * The name used by non-connected users can be changed thanks to userProvider.defaultName
 */
app.provider('messages', function() {

    this.$get = function() {

        messages = {
            error: {
                hasOne: false,
                title: "",
                message: ""
            },
            info: {
                hasOne: false,
                title: "",
                message: ""
            }
        }
            
        messages.setError = function(title, message) {
            messages.error.hasOne = true;
            messages.error.title = title;
            messages.error.message = message;
            
            setTimeout( function() { 
                messages.error.hasOne = false;
                messages.error.title = "";
                messages.error.message = "";
            }, 4000);
        }
            
        messages.setInfo = function(title, message) {
            messages.info.hasOne = true;
            messages.info.title = title;
            messages.info.message = message;
            
            setTimeout( function() { 
                messages.info.hasOne = false;
                messages.info.title = "";
                messages.info.message = "";
            }, 4000);
        }
        
        return messages;
    }
    
});