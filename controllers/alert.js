app.directive("error", [ '$http', 'messages', function($http, messages){
    return {
        restrict: 'E',
            templateUrl : 'views/alerts/error.html',
            scope: {
            
            },
            link: function($scope) {
                $scope.messages = messages;
            }
        };
    }
]);

app.directive("info", [ '$http', 'messages', function($http, messages){
    return {
        restrict: 'E',
            templateUrl : 'views/alerts/info.html',
            scope: {
            
            },
            link: function($scope) {
                $scope.messages = messages;
            }
        };
    }
]);