app.directive("menubar", [ '$http', '$location', 'connection', 'user', function($http, $location, connection, user){
    return {
        restrict: 'E',
            templateUrl : 'views/menubar.html',
            scope: {
                
            },
            link: function($scope) {
                
                $scope.user = user;
                $scope.connection = connection;

                // used by the nav bar (see menubar directive)
                // in order to select the active item
                $scope.isActive = function (viewLocation) { 
                    return viewLocation === $location.path();
                };
            }
        };
    }
]);