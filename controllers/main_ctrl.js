/* ------ ROUTES ------ */ 

app.config(function($routeProvider) {
    $routeProvider
    .when('/home', {
        templateUrl : 'views/welcome.html', 
        controller:'home',
        require_connection : false
    })
    .when('/gameplay', {
        templateUrl : 'views/gameplay.html',
        controller: 'gameplay',
        require_connection : true
    })
    .when('/matchmaking', {
        templateUrl : 'views/matchmaking.html',
        controller: 'matchmaking',
        require_connection : true
    })
    .when('/decks', {
        templateUrl : 'views/decks.html',
        controller: 'decks',
        require_connection : true
    })
    .when('/current_match/:param', {
        templateUrl : 'views/current_match.html',
        controller: 'current_match',
        require_connection : true
    })
    .when('/end_game/:param', {
        templateUrl : 'views/endgame.html',
        controller: 'end_game',
        require_connection : true
    })
    .otherwise({
    	redirectTo: '/home'
    });
})

/*
 * Add a listener that ensures that a user is connected before
 * letting it access a restricted route.
 */
app.run(function ($rootScope, user, messages) {
    $rootScope.$on('$routeChangeStart', function (event, next) {
        
        if( next.require_connection && ! user.isConnected ) {
            event.preventDefault(); // has no effect
            
            window.location.href = '#';
            
            messages.setError('Accès interdit', 'vous devez être connecté');
        }

    });
});

app.config(function($provide) {
  $provide.decorator('$log', function($delegate, $sniffer, user) {
        var _log = $delegate.log; //Saving the original behavior

        $delegate.log = function(message) { };
       // $delegate.warn = function(msg) {
            //alert(user.isConnected);
        //}

        return $delegate;
    });
})


/* --- MAIN CONTROLLER --- */

app.controller('main_ctrl', ['$scope', '$http', '$timeout', 'messages', function($scope, $http, $timeout, messages) {

    $scope.user = user;
    
    $scope.$on('connection', function(event) {
        $('#signin-modal').modal('hide');  // else, a greyish veil stays and you can't do much but refresh the page
        window.location.href="#!/gameplay";
        messages.setInfo('Connection réussie', 'Salut, ' + user.name);
    });
    
    $scope.$on('disconnection', function(event) {
        window.location.href='#';
        messages.setInfo('Success', 'Vous avez été déconnecté');
    });

    $scope.$on('subscription', function(event, data) {
        $('#signup-modal').modal('hide')
        
        window.location.href="#";
        messages.setInfo('Inscription réussie', 'Dépêche-toi de venir jouer ! ;)');
    });
    
    $scope.$on('delete_account', function(event, data) {
        $('#delete-account-modal').modal('hide');

        window.location.href='#';
        messages.setInfo('Suppression réussie', "traître :'(");
    });

 }]);
