
/*--- MATCHMAKING.HTML CONTROLLER (AFTER CLICKING ON THE 'PLAY' BUTTON IN GAMEPLAY.HTML) --- */
// includes automatically participating to the game, getting the available players,
// sending and accepting gaming requests.


routeAppControllers.controller('matchmaking', ['$scope', '$http', 'user', 'messages', function($scope, $http, user, messages) {
    
  // $scope.matchmaking_style = {'background': 'urnl(img/bg_test.jpg) no-repeat', 'background-size': 'cover'} // i keep this line, even tho it's
                                                                                    // currently useless, in case it turns out being useful one day
    $scope.no_requests = false;
    $scope.no_players = false;
    $scope.request_sent = false;
    $scope.wait = true;

    $scope.get_participation = function() {
        if( !user.isConnected )
            return;
        
        // participating to the game and retrieving gaming requests other players sent us:
        $http.get("https://amarger.murloc.fr:8080/matchmaking/participate?token=" + user.token)
              .then(function(response){
                if (response.data.status == "error") {
                    console.log("participation failed : " + response.data.message)
                    $scope.wait = false;
                    return; // commented because it always fails but i kinda want to test the rest of my code
                }
                //data.match says someone accepted your request
                var requests = response.data.data.request  // we'll use this when the session goddamn works
                
                if (requests.length == 0) {
                  $scope.no_requests = true; // prints a "nobody-loves-you"-but-a-bit-more-polite kind of message
                } else {
                    $scope.no_requests = false;
                    $scope.gamers_requests = requests
                }

                if (response.data.data.hasOwnProperty('match')) {
                    $scope.wait = false;

                    if (response.data.data.match.player1.name == user.name)
                      window.location.href="#!/current_match/player1";
                    else
                      window.location.href="#!/current_match/player2";
                } else {
                    setTimeout($scope.get_participation, 5000);
                }
        });
    }

    $scope.get_participation();

    $scope.get_players = function() {
        if( ! user.isConnected )
            return;
        
         // getting the list of all the available players :
        $http.get("https://amarger.murloc.fr:8080/matchmaking/getAll?token=" + user.token)
              .then(function(response){
                if (response.data.status == "error") {
                    console.log("get players failed : " + response.data.message)
                    return; // commented because it always fails but i kinda want to test the rest of my code
                }

                var players = response.data.data  // we'll use this when the session goddamn works
                // var players = [{"email":"aze@yopmail.com","name":"aze","matchmakingId":"58b5e10b7ff67d7fecfa6452"}]

                if (players.length == 0) {
                  $scope.no_players = true; // prints a "nobody-loves-you"-but-a-bit-more-polite kind of message
                } else {
                  $scope.available_players = players
                }
                if ($scope.wait) {
                    setTimeout($scope.get_players, 5000);
                }
        });
    }

    $scope.get_players();

    $scope.accept_request = function(match_id) {
        if( ! user.isConnected )
            return;
        
      $http.get("https://amarger.murloc.fr:8080/matchmaking/acceptRequest?matchmakingId=" + match_id + "&token=" + user.token)
          .then(function(response){
              if (response.data.status == "error") {
                  console.log("request acceptance failed : " + response.data.message)
                  return; // commented because it always fails but i kinda want to test the rest of my code
              }
              var players = response.data.data;
              window.location.href="#!/current_match/player2";
      });
    };

    $scope.send_request = function(match_id) {
        if( ! user.isConnected )
            return;
        
      $http.get("https://amarger.murloc.fr:8080/matchmaking/request?matchmakingId=" + match_id + "&token=" + user.token)
          .then(function(response){
              if (response.data.status == "error") {
                  console.log("request sending failed : " + response.data.message)
                  return; // commented because it always fails but i kinda want to test the rest of my code
              }
              
              $scope.request_sent = true;
              
              messages.setInfo("Demande envoyée", "Le match commencera dès que le joueur aura accepté votre demande. Ceci dit, il peut également vous ignorer ; dans ce cas, regardez le ciel sans cligner des yeux pour retenir vos larmes.");
      });
    };

 }]);