
/*--- GAMEPLAY.HTML CONTROLLER (WHEN YOU SUCCESSFULLY LOG IN) --- */
// includes logging out and deleting your account.

routeAppControllers.controller('gameplay', ['$scope', '$http', 'user', 'connection', function($scope, $http, user, connection) {
    
    $scope.user = user;
    $scope.connection = connection;
}]);