
/*--- DECK.HTML CONTROLLER (ONCE CONNECTED) --- */
// displays decks.

routeAppControllers.controller('decks', ['$scope', '$http', '$routeParams', '$timeout', 'user', 'messages', 'deck', function($scope, $http, $routeParams, $timeout, user, messages, deck_mgr) {
    
    $scope.mgr = deck_mgr;
    $scope.decks = deck_mgr.decks;
    $scope.selectedDeck = $scope.decks.length > 0 ? $scope.decks[0] : null;
    
    $scope.allCards = deck_mgr.getAllCards();
    
    $scope.cardsNotInDeck = [];
    
    $scope.editing = false;
    $scope.action = "Edit";

    var show_button = function() {
        $scope.show_card = true;
        $timeout(show_button, 1200);
    }

    $timeout(show_button, 1200);
    
    $scope.selectDeck = function(deck) {
        $scope.selectedDeck = deck;
        $scope.deckCards = deck['cards'];
        $scope.editing = false;
        $scope.show = false;
        $scope.action = "Edit";
    }
    
    /*$scope.remove_deck = function(deck_to_remove) {
        $scope.decks = $scope.decks.filter(function(deck) {
            return deck.name != deck_to_remove.name;
        });
        
        // window.localStorage.setItem("decks", JSON.stringify($scope.decks));
    }*/
    
    $scope.editOrSave = function() {
        if( $scope.selectedDeck == null )
            return;
        
        // save current deck
        if( $scope.editing ) {
            window.localStorage.setItem("decks", JSON.stringify($scope.decks));
            $scope.action = "Edit";
        }    
        // edit current deck
        else {
            // keep the cards that are not within the deck
            $scope.cardsNotInDeck = $scope.allCards.filter(function(card) {
                return $scope.deckCards.find(function(el) {
                    return el['_id'] == card['_id'];
                }) == undefined;
            });
            
            $scope.editing = true;
            $scope.action = "Save";
        }
    }
    
    $scope.createANewDeck = function() {
        name = "newDeck";
        $scope.decks.push({
            'name': name,
            'cards': []
        });
        $scope.selectedDeck = $scope.decks.slice(-1)[0];
    }
    
    $scope.addCardToDeck = function(card) {
        if( ! $scope.editing )
            return;
        
        if( $scope.selectedDeck['cards'].length == 20 ) {
            messages.setError("Impossible d'ajouter " + card.name, "le deck est plein");
            return;
        }
        
        $scope.selectedDeck['cards'].push(card);
        $scope.cardsNotInDeck.splice($scope.cardsNotInDeck.indexOf(card), 1);
    }
    
    $scope.removeCardFromDeck = function(card) {
        if( ! $scope.editing )
            return;
        
        $scope.cardsNotInDeck.push(card);
        $scope.selectedDeck['cards'].splice($scope.selectedDeck['cards'].indexOf(card), 1);
    }
    
    // get all the cards
    $http.get("https://amarger.murloc.fr:8080/cards/getAll?token=" + user.token)
        .then(function(response){
            if (response.data.status == "error") {
                console.log("getting cards failed : " + response.data.message)
                return;
            }

            $scope.allCards = response.data.data;
    });
 }]); 