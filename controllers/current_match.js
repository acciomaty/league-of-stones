/*--- CURRENT_MATCH.HTML CONTROLLER (AFTER ACCEPTING A GAMING REQUEST OR HAVING A REQUEST ACCEPTED IN MATCHMAKING.HTML) --- */
// doesn't include anything yet.

routeAppControllers.controller('current_match', ['$scope', '$http', '$routeParams', "$rootScope", '$log', 'user', 'messages', function($scope, $http, $routeParams,$rootScope, $log, user, messages) {
    $scope.me = $routeParams.param;
    $scope.decks = JSON.parse(window.localStorage.getItem("decks"));
  
    $scope.wait = true;
    $scope.choose_deck = false;

    $scope.infos = "tu ne me connais pas encore :P";
    
    $scope.state = null;

    $scope.selectDeck = function(deck) {
        $scope.selectedDeck = deck;
        $scope.deckCards = [];
        deck['cards'].forEach(function(card) {
            $scope.deckCards.push("{\"key\":\"" + encodeURI(card.key) + "\"}")
        });
        var uri = $scope.deckCards.toString();

        $http.get("https://amarger.murloc.fr:8080/match/initDeck?deck=[" + uri + "]&token=" + user.token)
            .then(function(response){
                if (response.data.status == "error") {
                    $log.error("sending deck failed : " + response.data.message)
                    return;
                }

                $scope.infos = response.data.data;
        });
    }

    $scope.current_player = "";


    
    $scope.get_match_infos = function() {
        $http.get("https://amarger.murloc.fr:8080/match/getMatch?token=" + user.token)
            .then(function(response){
                if (response.data.status == "error") {
                    $log.error("getting match information failed : " + response.data.message)
                    return;
                }

                $scope.infos = response.data.data;
                var status = $scope.infos.status;
                //$scope.$apply;

                $scope.$broadcast("dataLoaded", response.data);
                
                if (status == "Deck is pending") {
                    $scope.wait = true;
                    $scope.choose_deck = true;
                } else {
                 
                    $scope.wait = false;
                    $scope.choose_deck = false;

                    if ($scope.infos[$scope.me].hp <= 0) {
                        console.log("end of the game, u lose !!!");
                        window.location.href="#!/end_game/loser";
                        return;
                    }

                    $scope.opponent = ($scope.me == "player1") ? $scope.infos["player2"] : $scope.infos["player1"];

                    if ($scope.opponent.hp <= 0) {
                        console.log("end of the game, u won !!!");
                        $scope.ending = function() {
                            $http.get("https://amarger.murloc.fr:8080/match/finishMatch?token=" + user.token)
                                .then(function(response){
                                    if (response.data.status == "error") {
                                        return;
                                    }
                                    window.location.href="#!/end_game/winner";
                                    return;
                            });
                        }
                        setTimeout($scope.ending, 2500);  // so the other player has time to refresh his page
                    }
                    
                    if (! $scope.infos[$scope.me].turn) {
                        if( $scope.current_player == "me" )
                            messages.setInfo('Fin de tour', "Vous avez terminé")
                        $scope.current_player = "other";
                        $log.error("C'est au tour de l'adversaire");
                    }
                    else {
                        if( $scope.current_player != "me" )
                            messages.setInfo("Début du tour", "Votre tour a commencé")

                        $scope.current_player = "me";

                        $scope.state = "pick_card";
                            
                        $http.get("https://amarger.murloc.fr:8080/match/pickCard?token=" + user.token)
                            .then(function(response){
                                if (response.data.status == "error") {
                                   //$log.error("picking a card failed : " + response.data.message)
                                    return;
                                }
                                $scope.state = "play";
                        });
                    }
                }
                
                setTimeout($scope.get_match_infos, 2500);

            });
    }

    $scope.get_match_infos();
    
    
 }]);