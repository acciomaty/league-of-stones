/*--- WELCOME.HTML CONTROLLER (LANDING PAGE) --- */
// includes signing in and signing up.

routeAppControllers.controller('home', ['$scope', '$http', '$timeout', function($scope, $http, $timeout) {
    $scope.subscribed = false;
    $scope.erreur = false;

    var show_button = function() {
        $scope.show_card = true;
        $timeout(show_button, 1200);
    }

    $timeout(show_button, 1200);
 }]);