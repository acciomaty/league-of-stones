routeAppControllers.controller('end_game', ['$scope', '$routeParams', '$timeout', function($scope, $routeParams, $timeout) {
    $scope.status = $routeParams.param;
    var show_button = function() {
        $scope.show_card = true;
        $timeout(show_button, 1200);
    }

    $timeout(show_button, 1200);
 }]);