



app.directive('emitLastRepeaterElement', function() {
    return function(scope) {
        if (scope.$last){
            scope.$parent.$broadcast('LastRepeaterElement');
            console.log('on emet')
        }
    };
});




app.directive('cardPlaying', function ($http) {
    return {
        replace: false,     

        link: function($scope, elem, attrs) {
            
            
            var card,
                image,
                cardw,
                cardh,
                cardx,
                cardy,
                ocardx,
                ocardy,
                pinx,
                piny,
                pinxperc,
                pinyperc,
                targetx,
                targety,
                rx,
                ry,
                targetrx,
                targetry,
                scale,
                targetscale,
                ww,
                wh,
                md,
                mx,
                my,
                whoosh,
                whooshvol,
                whooshvoltarget,
                majesty,
                majestyvol,
                majestyvoltarget,
                audioloaded;
                var firstleft = ((attrs.index * 9) - 20 );
                var firsttop= 42;
                var disttop;
                var distleft;
                var premier = 0;
                var again = false;
            

                board = elem.parent().parent()[0].childNodes[3];
                // modification pour avoir un board qui change
                launch();
                bindevents();
                loop();


            function launch() {

                onresize();

                
                card = elem[0];
                image = elem[0].childNodes[1];
                cardw = image.clientWidth;
                cardh = image.clientHeight;
                cardx = ww / 2 - cardw / 2;
                cardy = wh / 2 - cardh / 2;
                ocardx = cardx;
                ocardy = cardy;
                pinx = 0;
                piny = 0;
                pinxperc = 0;
                pinyperc = 0;
                targetx = cardx;
                targety = cardy;
                rx = 0;
                ry = 0;
                targetrx = 0;
                targetry = 0;
                scale = 1;
                targetscale = scale;
                md = false;
                mx = cardx;
                my = cardy;
                card.style.top= firsttop+'%'
                card.style.left = firstleft+'%'
    
            }

            function overlaps( a, b ) {
                    var pos1 = getPositions( a ),
                        pos2 = getPositions( b );
                    return comparePositions( pos1[0], pos2[0] ) && comparePositions( pos1[1], pos2[1] );
                }




                function getPositions( elem ) {
                    var pos, width, height;
                    pos = $(elem).position();
                    width = $(elem).width();
                    height = $(elem).height();
                    return [ [ pos.left, pos.left + width ], [ pos.top, pos.top + height ] ];
                }

                function comparePositions( p1, p2 ) {
                    var r1, r2;
                    r1 = p1[0] < p2[0] ? p1 : p2;
                    r2 = p1[0] < p2[0] ? p2 : p1;
                    return r1[1] > r2[0] || r1[0] === r2[0];
                }

              

            function bindevents() {

                //for pc
                elem.bind( 'mousedown', onmousedown );
                window.addEventListener( 'mouseup', onmouseup );
                window.addEventListener( 'mousemove', onmousemove );

                window.addEventListener( 'resize', onresize );
                
                
            }


            function onmousedown( e ) {
                md = true;
                mx = e.pageX;
                my = e.pageY;
                // pin to click point
                pinx = mx - cardx; 
                piny = my - cardy; 
                // transform based on the pin position
                pinxperc = 100 - ( pinx / cardw ) * 100; 
                pinyperc = 100 - ( piny / cardh ) * 100; 
            }

            function onmouseup() {
                md = false;
               
                //card played now!
                if (overlaps (board,card)){
                    unid= card.id
                    unimg = image.src
                    parent = card.parentNode
                    
                   
                    if($scope.ourBattlefield.length < 5){

                        obj= $scope.playablecard.find(function(el){
                            return el.key == unid;
                        });
                        console.log ("in playing card")
                        $http.get("https://amarger.murloc.fr:8080/match/playCard?card=" + obj.key + "&token=" + user.token)
                            .then(function(response){
                                if (response.data.status == "error") {
                                   $log.error("playing a card failed : " + response.data.message)
                                    return;
                                }
                                console.log(obj.key, " has been played ! ")
                                // $scope.infos[$scope.me].hand.push(response.data.data);
                        });

                        $scope.ourBattlefield.push(obj);

                        console.log($scope.ourBattlefield)

                        idaenlever =$scope.playablecard.indexOf(obj);
                        $scope.playablecard.splice(idaenlever,1)


                        // will be done when a new card will be created    
                     }else{
                        mx= disttop +pinx;
                        my= distleft+piny;
                     }
                      $scope.$apply();
                }



            }

            function onmousemove( e ) {
                if( md ) {
                    mx = e.pageX;
                    my = e.pageY;
                }
            }

            function onresize() {
                ww = window.innerWidth;
                wh = window.innerHeight;
            }

            function loop() {
                requestAnimationFrame( loop )


                // c'est la qu'il faut look
                if(premier == 0){
                    distleft = my;
                    disttop = mx;
                    premier = 1;
                }
                

                    // set new target position
                    targetx = mx - cardx - pinx;
                    targety = my - cardy - piny;
                
                
                // lerp to new position
                cardx += targetx * 0.25;
                cardy += targety * 0.25;
                
                // contain card to window bounds
                if( cardx < -cardw / 2 ) {
                    cardx = -cardw / 2;
                }
                if( cardx > ww - cardw / 2 ) {
                    cardx = ww - cardw / 2;
                }
                if( cardy < -cardh / 2 ) {
                    cardy = -cardh / 2;
                }
                if( cardy > wh - cardh / 2 ) {
                    cardy = wh - cardh / 2;
                }
                
                // get rotation based on how much card moved
                targetrx = ( ocardy - cardy - rx ) * 2;
                targetry = ( cardx - ocardx - ry ) * 2;
                
                // lock rotation so things don't get too crazy
                targetrx = Math.min( targetrx, 40 );
                targetrx = Math.max( targetrx, -40 );
                targetry = Math.min( targetry, 40 );
                targetry = Math.max( targetry, -40 );
                
                // lerp to new rotation
                rx += targetrx * 0.1;
                ry += targetry * 0.1;
                    
                // scale up when the mouse is pressed
                targetscale = md ? 1.9 - scale : 1 - scale;
                scale += targetscale * 0.2;
                

                // apply the transform
                card.style.webkitTransform = 'translate3d(' + cardx + 'px, ' + cardy + 'px, 0)';
                image.style.webkitTransformOrigin = pinxperc + '% ' + pinyperc + '%';

                // store the old card position
                ocardx = cardx;
                ocardy = cardy;
            }





            





        }
    };
});



app.directive('pioche', [function() {
    return {
        replace: false,
        link: function(scope, elm, attrs) {
            elm.on('mouseenter',function() {
                    elm.css('opacity','1');
                });

            elm.on('mouseleave',function() {
                    elm.css('opacity','0');
                });
        }
    };
}]);


app.directive('playingGame', function ($log, $http) { 
    return {
        restrict: 'A',
        replace: true,
        scope : true, 
        templateUrl: 'views/playingGame.html',
        link: function($scope,$element) {
            
            $scope.range = function(num) {
                return new Array(num);   
            }
            
            // force auto-update of data
            // the event is sent from current_match when data about the match are received from the server.
            $scope.$on("dataLoaded", (event, data) => {
                $scope.player = ($scope.me == "player1") ? $scope.infos["player1"] : $scope.infos["player2"];
                $scope.opponent = ($scope.me == "player1") ? $scope.infos["player2"] : $scope.infos["player1"];

                $scope.carteAdverse= $scope.opponent.hand
                $scope.playablecard = $scope.player.hand
                // when we play card, it's here
                $scope.ourBattlefield= $scope.player.board;

                // when the ennemy play a card, it's here
                $scope.ennemyBattlefield = $scope.opponent.board;
                
                $scope.decks = [{
                    id: 1,
                    img : "img/dos_carte.png",
                    top : 35,
                    cardleft: $scope.opponent.deck,
                    hp:$scope.opponent.hp,
                },{
                    id: 2,
                    img : "img/dos_carte.png",
                    top: 100-40,        
                    cardleft: $scope.player.deck,
                    hp:$scope.player.hp,
                }
            ];   
            });
            
            $scope.player = ($scope.me == "player1") ? $scope.infos["player1"] : $scope.infos["player2"];
            $scope.opponent = ($scope.me == "player1") ? $scope.infos["player2"] : $scope.infos["player1"];

            $scope.carteAdverse= $scope.opponent.hand
            $scope.playablecard = $scope.player.hand
            $scope.elemSelected = ''
            
            console.log($scope.player)
            console.log($scope.opponent)
           
            // when we play card, it's here
            $scope.ourBattlefield= $scope.player.board;

            // when the ennemy play a card, it's here
            $scope.ennemyBattlefield = $scope.opponent.board;

            // we're attacking the person if the doesn't have any champions on board
            // if ( $scope.ennemyBattlefield.length == 0) {
            //     $http.get("https://amarger.murloc.fr:8080/match/attack?card=" + $scope.cardSelected.key + "&ennemyCard=" + card.key + "&token=" + user.token)
            //         .then(function(response){
            //             if (response.data.status == "error") {
            //                $log.error("attacking failed : " + response.data.message)
            //                 return;
            //             }
            //             console.log("Votre carte ", $scope.cardSelected.key, "a attaqué ", card.key, " !")
            //     });
            // }

            //juste need to get the cardleft from the WS
            $scope.decks = [{
                    id: 1,
                    img : "img/dos_carte.png",
                    top : 35,
                    cardleft: $scope.opponent.deck,
                    hp:$scope.opponent.hp,
                },{
                    id: 2,
                    img : "img/dos_carte.png",
                    top: 100-40,        
                    cardleft: $scope.player.deck,
                    hp:$scope.player.hp,
                }
            ];   

            $scope.attack_opponent = function() {
                $http.get("https://amarger.murloc.fr:8080/match/attackPlayer?card=" + $scope.cardSelected.key + "&token=" + user.token)
                    .then(function(response){
                        if (response.data.status == "error") {
                           $log.error("attacking opponent failed : " + response.data.message)
                            return;
                        }
                        console.log("Votre carte ", $scope.cardSelected.key, "a attaqué l'adversaire !")
                });
            }

            $scope.attack= function(card){
                console.log("c'est obj 1 qui attaque obj2",$scope.cardSelected,card);
                
                $http.get("https://amarger.murloc.fr:8080/match/attack?card=" + $scope.cardSelected.key + "&ennemyCard=" + card.key + "&token=" + user.token)
                    .then(function(response){
                        if (response.data.status == "error") {
                           $log.error("attacking failed : " + response.data.message)
                            return;
                        }
                        console.log("Votre carte ", $scope.cardSelected.key, "a attaqué ", card.key, " !")
                });

                $scope.mainPlayerAction= false;
                $scope.elemSelected.parentElement.style.border='1px solid black';
                $scope.cardSelected = '';
                $scope.elemSelected ='';
            }



            $scope.mainPlayerAction = false;

            $scope.cardActivate= function(card,event){
                if(!$scope.mainPlayerAction){
                    console.log(card)
                    console.log(event.target)
                    // don't know why style apply to son
                    $scope.elemSelected = event.target
                    $scope.cardSelected = card;
                    $scope.mainPlayerAction= true;
                    event.target.parentElement.style.border ="1px solid red" ;
                }else{

                    if($scope.elemSelected == event.target){
                        $scope.mainPlayerAction= false;
                        $scope.elemSelected.parentElement.style.border='1px solid black';
                        $scope.cardSelected = '';
                        $scope.elemSelected ='';
                    }

                }
            };

            $scope.finish_turn = function() {
                if ($scope.player.turn) {
                    $http.get("https://amarger.murloc.fr:8080/match/endTurn?token=" + user.token)
                            .then(function(response){
                                if (response.data.status == "error") {
                                   $log.error("ending turn failed : " + response.data.message)
                                    return;
                                }
                                console.log("Vous avez fini votre tour !")
                        });
                    return;
                }
                console.log("Ce n'est pas votre tour !")
            }
        }
    };
});



app.directive('basicClick', function($parse, $rootScope) {
  return {
    compile: function(elem, attr) {
      var fn = $parse(attr.basicClick);
      return function(scope, elem) {
        elem.on('click', function(e) {
          fn(scope, {$event: e});
          scope.$apply();
        });
      };
    }
  };
});




app.directive("cardPlayedImage", [ '$http', function($http){
      return {
          restrict: 'E',
          templateUrl : 'views/cardPlayedImage.html',
          scope: {
            img: '='
          },
          link: function($scope) {
          }
       };
  }]);